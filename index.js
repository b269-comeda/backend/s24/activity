let num = 2
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

let address = ['258', 'Washington Ave NW', 'California', '90011'];

// Destructuring the address array
let [unitNumber, streetName, state, zipCode] = address;
console.log(`I live at ${unitNumber} ${streetName}, ${state} ${zipCode}`);


let animal = {
	name: 'Lolong',
	type: 'saltwater crocodile',
	weight: '1075 kgs',
	length: '20 ft 3 in'
};

// Destructuring animal object
let {name, type, weight, length} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${length}.`);


let numbers = [1, 2, 3, 4, 5];

// looping through number array using forEach and arrow function
numbers.forEach((number) => {
	console.log(number);
});


let reduceNumber = numbers.reduce((x, y) => x + y, 0);
console.log(reduceNumber);


class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);